'Skypicker API'

import requests
import json

FLIGHTS_URL = 'https://api.skypicker.com/flights'

def flights(params: dict) -> dict:
    'get http request to api server without check params'

    # print(f'search flights on {FLIGHTS_URL}')

    resp = requests.get(FLIGHTS_URL, params=params)

    status_code = resp.status_code
    # print(f'status {status_code}')

    # print(resp.url)

    if status_code >= 400:
        print(f'ERROR {resp.content}')
        return

    data = resp.json()
    # print(json.dumps(data['data'], indent=2))

    return data