#!/usr/bin/env python3

import click
from click_datetime import Datetime
from datetime import date, datetime, timedelta

from SkypickerApi import flights
from Booking import booking

@click.command()
# @TODO: default=date.today() asi není dobrý nápad, letenka není jízdenka na šalinu
@click.option('--date', 'date_', type=Datetime(format='%Y-%m-%d') , prompt='datum odletu rok-měsíc-den 2018-04-13', help = 'datum odletu rok-měsíc-den např. 2018-04-13')
@click.option('--from', 'from_',prompt='From (IATA code):', help = 'z, parametry potřebují podporu letiště IATA codes')
@click.option('--to', prompt='To (IATA code):', help = 'do, parametry potřebují podporu letiště IATA codes')
@click.option('--one-way', is_flag=True, default=True, help = '(z tohoto udělej default option) indikuje potřebu zákazníka letět jenom jedním směrem, tedy pouze do nějakého místa')
@click.option('--return', 'return_', default=0,type=int, help = 'return 5 by měla zabookovat let s cestujícím, který v destinaci zůstává 5 nocí')
@click.option('--cheapest' ,default = True, help = '(default) zabookuje nejlevnější let a --fastest bude fungovat stejně')
# @todo: @click.option('--cheapest/--fastest' => TypeError: main() missing 1 required positional argument: 'fastest' ???
@click.option('--fastest', default = False, help = 'zabookuje nejrychlejší let')
# bags bude zadávat až po nalezení letu, bo není parametrem vyhledávání letu
# @click.option('--bags', default = 0, help = 'bags 2 by měla zabookovat let se 2 zavazadly')
def main(date_, from_, to, one_way, return_, cheapest, fastest):
    
    

    # print(date_, from_, to, one_way, return_, cheapest, fastest)
    print('hledám let')
    print(f'from {from_} to {to} date {date_} cheapest {cheapest} fastest {fastest} one_way {one_way} return {return_}  ')

    # @TODO: pretty errors messages
    assert cheapest is not fastest, f'--cheapest or --fastest ???'
    assert date_.date() > date.today(), f'--date nemože být v minulosti'
    if one_way is True:
        assert return_ == 0, f'žádáš pouze cestu tam, nedávej tedy datum návratu'
    else:
        # @TODO: může zpět ve stejný den, ale nemělo by to být výchozí hdonotou
        # assert return_ > 0, f'žádáš cestu zpět, zadej tedy datum návratu'
        pass

    params = {
        'dateFrom': date_.strftime('%d/%m/%Y'),
        'to': to,
        'flyFrom': from_,
        'limit': 1
    }

    if return_ > 0:
        params['returnFrom'] = (date_ + timedelta(days=return_)).strftime('%d/%m/%Y')

    if one_way is True:
        # @TODO: will be deprecated https://skypickerpublicapi.docs.apiary.io/#reference/flights/flights/get
        params['typeFlight'] = 'oneway'


    if cheapest is True:
        params['sort'] = 'price'
    elif fastest is True:
        params['sort'] = 'duration'

    flights_data = flights(params)

    print_flight(flights_data['data'][0])

    if not click.confirm('Rezervovat tento let?'):
        exit()

    
    # pprint(type(flights_data))
    # pprint(flights_data[0].keys())
    booking_response = booking(booking_token = flights_data['data'][0]["booking_token"])
    # pprint(booking_response)


def print_flight(flight):

    route = []
    for r in flight['route']:

        dTime = datetime.fromtimestamp(r['dTime'])
        aTime = datetime.fromtimestamp(r['aTime'])

        route.append(f'''{r['cityFrom']} {dTime} => {r['cityTo']} {aTime}''')

    route_itinerary = '\n'.join(route)

    print(f'''
==============================================================================
                YOUR FLIGHT

FROM {flight['countryFrom']['name']} {flight['cityFrom']} {flight['flyFrom']}
=>
TO {flight['countryTo']['name']} {flight['cityTo']} {flight['flyTo']}

FLY DURATION {flight['fly_duration']}
PRICE {flight['price']}

{route_itinerary}
==============================================================================
    ''')


if __name__ == '__main__':
    main()